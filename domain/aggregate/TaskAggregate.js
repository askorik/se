module.exports = function() {
  return {
    create: function(title, done) {
      this.$emitDomainEvent('TodoAdded', {
        title: title
      });
      return done();
    },
    remove: function() {
      return this.$emitDomainEvent('TodoRemoved');
    },
    complete: function() {
      return this.$emitDomainEvent('TodoCompleted');
    },
    incomplete: function() {
      return this.$emitDomainEvent('TodoNotCompleted');
    },
    changeTitle: function(title) {
      return this.$emitDomainEvent('TodoTitleChanged', {
        title: title
      });
    }
  };
};