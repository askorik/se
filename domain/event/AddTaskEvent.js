module.exports = { 
    var todoContext = require('ToDoContext');
    todoContext.defineDomainEvents({
        TodoCreated: function(params) {},
        TodoDescriptionChanged: function(params) {
            this.description = params.description;
        }
    });
};