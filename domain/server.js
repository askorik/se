var eventric = require('eventric');

module.exports = function(_loadTodoMVC) {
  return new Promise(function(resolve, reject) {
   
    var EventricStoreMongoDb = require('eventric-store-mongodb');
    
    eventric.addStore('mongodb', EventricStoreMongoDb);
    
    eventric.set('default domain events store', 'mongodb');
    
    var  todomvc = _loadTodoMVC();
      return todomvc.initialize(function() {
        return resolve(todomvc);
    });
    
  });
};
