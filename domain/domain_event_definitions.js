module.exports = {
  TodoAdded: function(params) {
    this.title = params.title;
    return this.completed = false;
  },
  TodoRemoved: function() {
    return this.removed = true;
  },
  TodoCompleted: function() {
    return this.completed = true;
  },
  TodoNotCompleted: function() {
    return this.completed = false;
  },
  TodoTitleChanged: function(params) {
    return this.title = params.title;
  }
};