module.exports = {
  AddTodo: function(params, done) {
    return this.$repository('Todo').create(params.title).then((function(_this) {
      return function(todoId) {
        return _this.$repository('Todo').save(todoId);
      };
    })(this)).then(function(todoId) {
      return done(null, todoId);
    });
  },
  RemoveTodo: function(params, done) {
    return this.$repository('Todo').findById(params.id).then((function(_this) {
      return function(todo) {
        todo.remove();
        return _this.$repository('Todo').save(params.id);
      };
    })(this)).then(function() {
      return done();
    });
  },
  CompleteTodo: function(params, done) {
    return this.$repository('Todo').findById(params.id).then((function(_this) {
      return function(todo) {
        todo.complete();
        return _this.$repository('Todo').save(params.id);
      };
    })(this)).then(function() {
      return done();
    });
  },
  IncompleteTodo: function(params, done) {
    return this.$repository('Todo').findById(params.id).then((function(_this) {
      return function(todo) {
        todo.incomplete();
        return _this.$repository('Todo').save(params.id);
      };
    })(this)).then(function() {
      return done();
    });
  },
  ChangeTodoTitle: function(params, done) {
    return this.$repository('Todo').findById(params.id).then((function(_this) {
      return function(todo) {
        todo.changeTitle(params.title);
        return _this.$repository('Todo').save(params.id);
      };
    })(this)).then(function() {
      return done();
    });
  }
};
