module.exports = function() {
  return {
    todos: [],
    handleTodoAdded: function(domainEvent) {
      var todo;
      todo = {
        id: domainEvent.aggregate.id,
        title: domainEvent.payload.title,
        completed: domainEvent.payload.completed
      };
      return this.todos.push(todo);
    },
    handleTodoCompleted: function(domainEvent) {
      return this.todos.map(function(todo) {
        if (todo.id === domainEvent.aggregate.id) {
          return todo.completed = true;
        }
      });
    },
    handleTodoRemoved: function(domainEvent) {
      return this.todos = this.todos.filter(function(todo) {
        return todo.id !== domainEvent.aggregate.id;
      });
    },
    handleTodoNotCompleted: function(domainEvent) {
      return this.todos.map(function(todo) {
        if (todo.id === domainEvent.aggregate.id) {
          return todo.completed = false;
        }
      });
    },
    handleTodoTitleChanged: function(domainEvent) {
      return this.todos.map(function(todo) {
        if (todo.id === domainEvent.aggregate.id) {
          return todo.title = domainEvent.payload.title;
        }
      });
    }
  };
};