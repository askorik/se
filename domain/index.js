var eventric = require('eventric');

var _loadTodoMVC = function() {
  var context = eventric.context('todomvc');
  context = context.defineDomainEvents(require('./domain_event_definitions'));
  context = context.addAggregate('Todo',  require('./aggregate/TaskAggregate'));
  context = context.addCommandHandlers(require('./commandHandler/CommandHandlers'));
  context = context.addProjection('Todos', require('./projection/TodoProjection'));
  return context;
};

module.exports = require('./server')(_loadTodoMVC);