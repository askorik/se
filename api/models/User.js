/**
 * User
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

      provider: 'STRING',
      uid_facebook: 'INTEGER',
      uid_google: 'INTEGER',

      username: 'string',
      name: 'STRING',
      email:{
          type: 'STRING',
          required: true
      },
      fistname: 'STRING',
      lastname: 'STRING',
      location: 'string',
      link: 'string',
      male: 'string',
      image: 'string',
    
  }

};
