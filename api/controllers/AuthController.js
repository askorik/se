/**
 * AuthController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var passport = require('passport');

module.exports = {

    index: function (req, res) {
        res.view();
    },
    test: function(req, res){
        res.view({layout: null});
    },
    logout: function (req, res) {
        req.logout();
        res.redirect('/');
    },
  /**
   * Action blueprints:
   *    `/auth/facebook`
   */
  facebook: function (req, res) {
      passport.authenticate('facebook', { failureRedirect: '/login', scope: ['email'] },
          function (err, user) {
              console.info(user);
              req.logIn(user, function (err) {
                  if (err) {
                      console.log(err);
                      res.view('500');
                      return;
                  }
                  passport.serializeUser(function(user, done) {
                      done(null, user);
                  });
                  res.redirect('/');
                  return;
              });
          })(req, res);
  },


  /**
   * Action blueprints:
   *    `/auth/googleplus`
   */
   google: function (req, res) {
      passport.authenticate('google', { successRedirect: '/',
          failureRedirect: '/login', scope: [
                  "https://www.googleapis.com/auth/userinfo.profile",
                  "https://www.googleapis.com/auth/userinfo.email"
              ], },
          function (err, user) {
              console.info(user);
              req.logIn(user, function (err) {
                  if (err) {
                      console.log(err);
                      res.view('500');
                      return;
                  }
                  passport.serializeUser(function(user, done) {
                      done(null, user);
                      cacccc});


                  res.redirect('/');
                  return;
              });
              })(req, res);

    // Send a JSON response
//    return res.json({
//      hello: 'world'
//    });
  },




  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to AuthController)
   */
  _config: {}

  
};
