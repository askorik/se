/**
 * UserController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var passport = require('passport');

module.exports = {
    
  
  /**
   * Action blueprints:
   *    `/user/index`
   *    `/user`
   */
   index: function (req, res) {
    var user = User.findOne();
    // Send a JSON response
    return res.view();
  },

    process: function(req, res) {

    },
  /**
   * Action blueprints:
   *    `/user/login`
   */
   login: function (req, res) {
      var userName = req.param('email');
      var pass = req.param('password');

      console.info('User added', userName, pass);
    // Send a JSON response
    return res.json({
      hello: 'world'
    });
  },

   registration: function(req, res) {
       var userName = req.param('email');
       var pass = req.param('password');

       User.create(
           {username:userName,
               email:userName,
               password:passwordHash.generate(pass)
           },
           function(error, model) {
            if(error) {
                return console.error('Regisstration error ' + error);
            } else {
                console.info('User added', model);
            }
       });
       return res.json({
           request: 'success'

       })
   },


  /**
   * Action blueprints:
   *    `/user/logout`
   */
   logout: function (req, res) {
    
    // Send a JSON response
    return res.json({
      hello: 'world'
    });
  },


    facebook: function (req, res) {
//        passport.authenticate('facebook', { scope: 'read_stream' });
    },





  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
  _config: {}

  
};
