/**
 * MainController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
  
  /**
   * Action blueprints:
   *    `/main/index`
   *    `/main`
   */
   index: function (req, res) {
    
    // Send a JSON response
    return res.view();
  },


  /**
   * Action blueprints:
   *    `/main/add`
   */
   add: function (req, res) {

      var timeNow = new Date();
      var commentText = req.param('text');
      Textrec.create({
          text: commentText,
          time:timeNow
      }).done(
          function(error, comment) {
              if (error) {
                  return console.log(error);

              } else {
                  console.log("Comment added:", comment);
              }
          });
    return res.json({
      hello: 'world'
    });
  },


  /**
   * Action blueprints:
   *    `/main/all`
   */
   all: function (req, res) {
    
    Textrec.find().done(function(err, comments) {
          if (err) {
              return console.log(err);
          } else {
              res.json({
                  success: true,
                  request: comments
              });
          }

      });
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to MainController)
   */
  _config: {}

  
};
