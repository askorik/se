/**
 * Created by duxa on 8/13/2014.
 */
/**
 * MainController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var eventric = require('eventric');
var todoContext  = require('./../../domain/index');

module.exports = {
    
    index: function (req, res) {
        // Send a JSON response
       res.send('<p>some html</p>');
    },
    
    update: function(req, res){
        todoContext.then(function(context){
           context.command('AddTodo',{title: 'Create a TodoMVC template'}).then(function(todoId) {
                return context.command('CompleteTodo', {
                    id: todoId
                });
            });
        });
    }
};

