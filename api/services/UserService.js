var UserService = {

    create: function(data) {
        var userData, isNewUser = false;
        userData = data;
        User.findOne({'email': userData.email}).done(function(err, user) {

            if(error) {
                isNewUser = true;
            } else {
                isNewUser = false;
            }
        });
    },
    addUserFromAuth: function(profileData, done) {
        console.log(profileData);

        User.findOne({'email': profileData.email}).done(function(err, user) {

            if(error) {
                isNewUser = true;
            } else {
                isNewUser = false;
            }
        });

        User.findOne({
                or: [
                    {uid: parseInt(profileData.id)},
                    {uid: profileData.id}
                ]
            }
        ).done(function (err, user) {
                if (user) {
                    return done(null, user);
                } else {

                    this.convertAuthToArray(profileData, data);

                    User.create(data).done(function (err, user) {
                        return done(err, user);
                    });
                }
            });

    },
    convertAuthToArray: function(source, destination) {

        var profileData = source;

        var data = destination;
        data = {
            provider: profileData.provider,
            uid: profileData.id,
            name: profileData.displayName
        };

        if(profileData.emails && profileData.emails[0] && profileData.emails[0].value) {
            data.email = profileData.emails[0].value;
        }
        if(profileData.name && profileData.name.givenName) {
            data.fistname = profileData.name.givenName;
        }
        if(profileData.name && profileData.name.familyName) {
            data.lastname = profileData.name.familyName;
        }

        return data;

    },
    getUserById: function(id) {

    },

    getUserByEmail: function(email) {
      return User.findOne({email: email});
    },

    updateUser: function(data) {

    },
};

module.exports = UserService;