module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            compile: {
                options: {
                   baseUrl: "./static",
                   removeCombined: true,
                   mainConfigFile :'./static/App/main.js',
                   //skipDirOptimize: true,
                   name: 'App/main',
                   include: [
                     "App/Directive/MenuMainTop/MenuMainTopDirective"
                    ],
                   paths: {},
                    generateSourceMaps: true,
                    preserveLicenseComments: false,
                    findNestedDependencies: true,
                    optimize: "uglify2",
                    out: './static/app.js'
                }
            }
        }
    });

    grunt.registerTask('default', [
        'requirejs'
    ]);
    
     grunt.registerTask('prod', [
        'requirejs'
    ]);
    
    
}