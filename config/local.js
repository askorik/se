var fs = require('fs'),
   lodash = require('lodash');

// config.local.js
module.exports = (function () {
  var defaults = {
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 1337,
    config: {
      paths: {
        environments: __dirname + '/environments'
      }
    }
  };

  var envConfigPath = defaults.config.paths.environments + '/' + defaults.env + '.js';
  var environment = {};

  if (fs.existsSync(envConfigPath)) {
    var environment = require(envConfigPath);
    //.info('Loaded environment config for ' + defaults.env + '.');
  } else {
   // logger.warn('Environment config for ' + defaults.env +' not found.');
  }
  return lodash.merge(defaults, environment);
}());