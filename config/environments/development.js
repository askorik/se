module.exports = {
    host: process.env.IP,
    port: process.env.PORT,
    adapters: {
        'default': 'mongo',
        mongo: {
            module   : 'sails-mongo',
            // url: "mongodb://process.env.OPENSHIFT_MONGODB_DB_USERNAME:process.env.OPENSHIFT_MONGODB_DB_PASSWORD@process.env.OPENSHIFT_MONGODB_DB_HOST:process.env.OPENSHIFT_MONGODB_DB_PORT/ghost",
            //schema: true,
            module   : 'sails-mongo',
            host     :  process.env.IP,
            port     : 27017,
            database : 'se',
            //user: process.env.OPENSHIFT_MONGODB_DB_USERNAME,
            //password: process.env.OPENSHIFT_MONGODB_DB_PASSWORD
        }
    },
    session: {
        url: 'mongodb://process.env.IP:27017/se/sessions',
        auto_reconnect: true
    }
};