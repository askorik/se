module.exports = {
    host: process.env.OPENSHIFT_NODEJS_IP,
    port: process.env.OPENSHIFT_NODEJS_PORT,
    adapters: {
        'default': 'mongo',
        mongo: {
            module   : 'sails-mongo',
            // url: "mongodb://process.env.OPENSHIFT_MONGODB_DB_USERNAME:process.env.OPENSHIFT_MONGODB_DB_PASSWORD@process.env.OPENSHIFT_MONGODB_DB_HOST:process.env.OPENSHIFT_MONGODB_DB_PORT/ghost",
            //schema: true,
            module   : 'sails-mongo',
            host     : process.env.OPENSHIFT_MONGODB_DB_HOST,
            port     : process.env.OPENSHIFT_MONGODB_DB_PORT,
            database : 'se',
            user: process.env.OPENSHIFT_MONGODB_DB_USERNAME,
            password: process.env.OPENSHIFT_MONGODB_DB_PASSWORD
        }
    },
    session: {
        url: 'mongodb://process.env.OPENSHIFT_MONGODB_DB_USERNAME:process.env.OPENSHIFT_MONGODB_DB_PASSWORD@$process.env.OPENSHIFT_MONGODB_DB_HOST:rocess.env.OPENSHIFT_MONGODB_DB_PORT/se/sessions',
        auto_reconnect: true
    }
};