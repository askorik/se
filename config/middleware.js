var passport = require('passport')
    , FacebookStrategy = require('passport-facebook').Strategy
    , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var express = require('express');


var verifyHandler = function (token, tokenSecret, profile, done) {
    process.nextTick(function () {
console.log(profile);
        UserService.addUserFromAuth(profile,done);

//        User.findOne({
//                or: [
//                    {uid: parseInt(profile.id)},
//                    {uid: profile.id}
//                ]
//            }
//        ).done(function (err, user) {
//                if (user) {
//                    return done(null, user);
//                } else {
//
//                    var data = {
//                        provider: profile.provider,
//                        uid: profile.id,
//                        name: profile.displayName
//                    };
//
//                    if(profile.emails && profile.emails[0] && profile.emails[0].value) {
//                        data.email = profile.emails[0].value;
//                    }
//                    if(profile.name && profile.name.givenName) {
//                        data.fistname = profile.name.givenName;
//                    }
//                    if(profile.name && profile.name.familyName) {
//                        data.lastname = profile.name.familyName;
//                    }
//
//                    User.create(data).done(function (err, user) {
//                            return done(err, user);
//                        });
//                }
//            });
    });
};
//
//passport.serializeUser(function (user, done) {
//    done(null, user.uid);
//});
//
//passport.deserializeUser(function (uid, done) {
//    User.findOne({uid: uid}).done(function (err, user) {
//        done(err, user)
//    });
//});


module.exports = {

    // Init custom express middleware
    express: {
        customMiddleware: function (app) {
            
            app.use('/static', express.static(process.cwd() + '/static'));

            passport.use(new FacebookStrategy({
                    clientID: "271996212963183",
                    clientSecret: "1045290126961a4902178221a87f4519",
                    callbackURL: "http://localhost:1337/auth/facebook/callback"
                },
                verifyHandler
            ));

            passport.use(new GoogleStrategy({
                    clientID: '1006885032307-jb8ibsks6vlqv38fd27ts1kofbompd76.apps.googleusercontent.com',
                    clientSecret: '9nqwcVbTEo9dkaQN7hBLyKmf',
                    callbackURL: 'http://localhost:1337/auth/google/callback'
                },
                verifyHandler
            ));

            app.use(passport.initialize());
            app.use(passport.session());
            
            
        }
    }
};


