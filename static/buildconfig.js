({                  
    baseUrl: ".",
    //removeCombined: true,
    mainConfigFile :'./App/main.js',
    //skipDirOptimize: true,
    name: 'App/main',
    paths: {},
    include: [
        "App/Directive/MenuMainTop/MenuMainTopDirective"
    ],
    generateSourceMaps: true,
    preserveLicenseComments: false,
    findNestedDependencies: true,
    optimize: "uglify2",
    out: 'app.js'
})