/**
 * Created by Administrator on 2/5/14.
 */
(function () {
    require.config({
        baseUrl: "/static",
        urlArgs: 'v='+ 1,
        waitSeconds: 200,
        paths: {
            "App":          '../static/App',
            "Directive":    '../static/App/Directive',
            "Services":     'App/services',
            'angular':      'lib/angular/angular',
            'ngRoute':      'lib/angular/angular-route',
            'ngCookies':    'lib/angular/angular-cookies',
            'bootstrap':    'lib/bootstrap/js/bootstrap',
            'jquery':       'lib/jquery/jquery-2.1.0',
            'angularAMD':   'lib/angular/angularAMD',
            'ngload':       'lib/angular/ngload',
            'text':         'lib/requirejs/require-txt',
            'less':         'lib/requirejs/require-less',
            'socket.io':     'lib/socket/socket.io',
            'sails.io':      'lib/socket/sails.io'
        },
        map: {
            '*': {
                'less': 'lib/requirejs/require-less'
            }
        },
        shim: {
            'jquery': {
                exports: '$'
            },
            'angular': {
                exports: 'angular'
            },
            'ngRoute': {
                deps: ['angular']
            },
            'ngCookies': {
                deps: ['angular']
            },
            'angularAMD': ['angular'],
            'ngload': ['angularAMD'],
            'bootstrap': {
                deps: ['jquery']
            },
            'sails.io': {
                deps: ['socket.io'],
                exports: 'io'
            }
        },
        deps: ['App/bootstrap']
    });
})();