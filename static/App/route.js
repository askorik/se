/**
 * Created by Administrator on 2/6/14.
 */
define(['angularAMD'], function( angularAMD ){
    return function Route(){

        this.init = function(app){
            app.config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider){
                $locationProvider.html5Mode(true);
                $routeProvider
                    .when('/test',  angularAMD.route({
                        templateUrl: '/static/App/Pages/LoginPage/LoginPageView.html',
                        controller: 'LoginPageController',
                        controllerUrl: '/static/App/Pages/LoginPage/LoginPageController.js'
                    }))
                    .when('/', angularAMD.route({
                        templateUrl: '/static/App/Pages/HomePage/HomePageView.html',
                        controller: 'HomePageController',
                        controllerUrl: '/static/App/Pages/HomePage/HomePageController.js'
                    }))
                    .otherwise({ redirectTo: '/home' });
            }]);
        }
    };
    
});