/**
 * Created by Administrator on 2/6/14.
 */
define(['angularAMD', './routing'], function( angularAMD, Routing ){
    var app = angular.module('shopExpressAdmin', []);
    var routing = new Routing(app);
    angularAMD.bootstrap(app);
    return app;
});