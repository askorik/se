/**
 * Created by Administrator on 2/5/14.
 */
define([
    'angularAMD',
    'angular',
    'App/app'
], function (angular) {
    'use strict';
    angular.bootstrap(document, ['ShopExpressAdmin']);
});
