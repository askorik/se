/**
 * Created by duxa on 4/19/2014.
 */
define(['./LoginPanelController', 'text!./LoginPanelView', 'less!./LoginPanel.less'], function(LoginPanelController, LoginPanelView, LoginPanelLess){
    return function LoginPanelDirective( App ){
       var self = this;

       App.register.directive('loginPanel', function(){
           return {
             scope: {},
             restrict: 'AE',
             controller: LoginPanelController,
             replace: true,
             template: LoginPanelView,
             link: function(scope, elem, attr){
                 elem.bind('click', function(){

                 });
             }
           };
       });
    };
});