/**
 * Created by Administrator on 2/6/14.
 */
(function () {
    require.config({
        baseUrl: "/Scripts",
        urlArgs: 'v='+ Math.random(),
        paths: {
            "App":          '../static/App',
            "services":     '/App/services',
            'angular':      '/lib/angular/angular',
            'ngRoute':      '/lib/angular/angular-route',
            'ngCookies':    '/lib/angular/angular-cookies',
            'bootstrap':    '/lib/bootstrap/js/bootstrap',
            'jquery':       '/lib/jquery/jquery-2.0.0',
            'angularAMD':   '/lib/angular/angularAMD'
        },
        shim: {
            'jquery': {
                exports: '$'
            },
            'angular': {
                exports: 'angular'
            },
            'ngRoute': {
                deps: ['angular']
            },
            'ngCookies': {
                deps: ['angular']
            },
            'bootstrap': {
                deps: ['jquery']
            },
            'angularAMD': ['angular'],
            'ngload': ['angularAMD']
        },
        map: {
            '*': {
                'less': 'require-less/less' // path to less
            }
        },
        deps: ['App/bootstrap']
    });
})();