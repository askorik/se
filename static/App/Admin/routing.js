/**
 * Created by Administrator on 2/6/14.
 */
define(['App/Admin/Admin/Pages/Login/LoginPageController'], function( LoginPageController ){
    return function Routing(App){
        App.config(function($routeProvider){
            $routeProvider
                .when('/',  angularAMD.route({
                    templateUrl: 'Admin/Pages/Login/LoginPageView.html',
                    controller: 'LoginPageController',
                    controllerUrl: 'Admin/Pages/Login/LoginPageController'
                }))
                .otherwise({ redirectTo: '/' });
        });
    };
});