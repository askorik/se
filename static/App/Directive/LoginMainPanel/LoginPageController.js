/**
 * Created by duxa on 5/9/2014.
 */
define([], function() {
    return function LoginPanelController($scope, socket){
        $scope.password ="";
        $scope.username = "";

        $scope.submitFacebook = function(){
            window.location.href  = "/auth/facebook";
        };

        $scope.submitGoogle = function(){
            window.location.href  = "auth/google";
        };

        $scope.submitForm = function(){
            socket.post('/login', {email: $scope.username, password: $scope.password}, function(res) {
                if(res.error) {
                    console.log('Error with login');
                } else {
                    console.log('all ok');
                }
            });
        }
    };
});