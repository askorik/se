/**
 * Created by duxa on 5/9/2014.
 */
define(['App/app','./LoginPageController', 'text!./LoginPageView.html', 'less!./LoginPage.less',
    './LoginPageLink'],
    function(App, LoginPanelController, LoginPanelView, LinkBind){
        App.register.directive('loginPage', function(){
            return {
                //scope: {},
                restrict: 'AE',
                controller: LoginPanelController,
                replace: true,
                template: LoginPanelView,
                link:  LinkBind
            };
        });
    });