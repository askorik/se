/**
 * Created by duxa on 7/5/2014.
 */
define(['App/app','./MenuMainTopController', 'text!./MenuMainTopView.html', 'less!./MenuMainTop.less',
        './MenuMainTopLink'],
    function(App, MainMenuController, MenuMainTopView, MenuMainTopLink){
        App.register.directive('mainTopMenu', function(){
            return {
                //scope: {},
                restrict: 'AE',
                controller: MainMenuController,
                replace: true,
                template: MenuMainTopView,
                link:  MenuMainTopLink
            };
        });
    });