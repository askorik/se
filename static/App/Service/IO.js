/**
 * Created by duxa on 6/25/2014.
 */
define(['App/app', 'sails.io'], function(App, io) {
    App.register.factory('socket',  function () {
        var socket = io.connect();
        socket.on('connect', function socketConnected() {
            console.log('Socket is now connected');
        });
        return socket;
    });
})