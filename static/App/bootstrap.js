/**
 * Created by Administrator on 2/5/14.
 */
define([
    'angular',
    'angularAMD',
    'App/app',
    'App/route'
], function (angular, angularAMD, App, Route) {
    'use strict';
    var route = new Route();
    route.init(App);
    angularAMD.bootstrap(App);
    require(['App/Service/IO', 'bootstrap', 'less!css/font-awesome/font-awesome.less']);
});