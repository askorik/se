module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            compile: {
                options: {
                    baseUrl: "../dev/static",
                    removeCombined: true,
                    mainConfigFile :'./static/App/main.js',
                    name: 'App/main',
                    paths: {},
//                    modules: [
//                        {
//                            name: 'App/main',
//                            include: ['less!App/Admin/Directive/Menu/Menu.less']
//                        }
//                    ],
//                   modules: [
//                        {
//                           // include: ['less!App/Admin/Directive/Menu/Menu.less'],
//                            //exclude: ['lib/requirejs/normalize']
//                        },{
//
//                        }
//                    ],
                    generateSourceMaps: true,
                    preserveLicenseComments: false,
                    findNestedDependencies: true,
                    optimize: "uglify2",
                    out: './static/app.js'
                }
            }
        }
    });

    grunt.registerTask('default', [
        'requirejs'
    ]);
}